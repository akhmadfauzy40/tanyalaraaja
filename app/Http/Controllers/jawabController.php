<?php

namespace App\Http\Controllers;

use App\Models\Jawab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class jawabController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'content' => ['required'],
            'image' => ['mimes:png,jpg,jpeg', 'max: 2048'],
            'tanya_id' => ['required']
        ]);

        $jawab = new Jawab;

        if($request->hasFile('image'))
        {
            // Simpan foto baru
            $imagePath = $request->file('image')->store('images', 'public');
            $jawab->image = $imagePath;
        }

        $jawab->content = $request->content;
        $jawab->tanya_id = $request->tanya_id;
        $jawab->users_id = Auth::id();

        $jawab->save();

        return redirect('tanyajawab/'.$request->tanya_id)->with('success', 'Tanggapan Berhasil di Posting');

    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => ['required'],
            'image' => ['mimes:png,jpg,jpeg', 'max: 2048'],
        ]);

        $jawab = Jawab::find($id);

        if ($request->hasFile('image')) {
            // Hapus foto lama jika ada
            if ($jawab->image) {
                $oldImagePath = $jawab->image;
                if (Storage::exists($oldImagePath)) {
                    Storage::delete($oldImagePath);
                }
            }

            // Simpan foto baru
            $imagePath = $request->file('image')->store('images', 'public');
            $jawab->image = $imagePath;
        }

        $jawab->content = $request->content;
        $jawab->save();

        return redirect('tanyajawab/'.$request->tanya_id)->with('success', 'Tanggapan Berhasil di Update');

    }

    public function delete(Request $request, $id)
    {
        $jawab = Jawab::find($id);

        // Hapus foto jika ada
        if ($jawab->image) {
            $oldImagePath = $jawab->image;
            if (Storage::exists($oldImagePath)) {
                Storage::delete($oldImagePath);
            }
        }

        // Hapus data tanya
        $jawab->delete();

        return redirect('tanyajawab/'.$request->tanya_id)->with('success', 'Tanggapan Berhasil di Hapus.');
    }
}
