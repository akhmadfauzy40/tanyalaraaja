## Final Project

## Kelompok 7

## Anggota Kelompok

- Akhmad Fauzi (@akhmadfauzy40)
- Kornelius Rhesa Valdis Setyawan (@Kayveex)
- Muti'ah Nabilah (@mutiahnb)

## Tema Project

Forum Discuss Laravel 

## ERD

<center><img src="public/img/ERD_TanyaLaraAja.png" width="400"></center>

## Link Video

belum ada

## Template

Dashboard: [https://themewagon.com/themes/free-responsive-bootstrap-5-admin-template-material-dashboard-2/]

## Tutor Instalasi Web

- buat folder baru lalu buka terminal di folder tersebut lalu ketik perintah di bawah ini

```bash
git clone https://gitlab.com/akhmadfauzy40/tanyalaraaja.git
```

```bash
composer install
```

- buat file baru dengan nama ".env" lalu copy isi dari file ".env.example" dan paste ke dalam ".env"
- ubah keterangan database (DB_DATABASE), kalau bisa sesuaikan saja nama databasenya menjadi 'tanyalaraaja'
- buat database baru di phpmyadmin (gunakan XAMPP), buat saja databasenya dengan nama 'tanyalaraaja' lalu import file 'tanyalaraaja.sql' ke database yang sudah dibuat tadi
- lalu jalankan perintah di bawah ini

```bash
php artisan key:generate
```

```bash
php artisan storage:link
```

```bash
composer require laravel/ui --dev
```

- ketik "no" lalu "yes"

```bash
php artisan ui bootstrap
```

```bash
npm install
```

```bash
npm run dev
```

- buka cmd baru lalu ketik

```bash
php artisan serve
```
